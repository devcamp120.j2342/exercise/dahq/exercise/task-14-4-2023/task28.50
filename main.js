"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    $("#btn-submit").on("click", function () {
        onBtnSubmitData();
    });
    $("#btn-clear").on("click", function () {
        onBtnClear();
    })


})


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnSubmitData() {
    var vPersonObj = {
        firstName: "",
        lastName: "",
        age: 0,

    };
    thuThapThongTin(vPersonObj);


    var vCheck = kiemTraThongTin(vPersonObj);
    if (vCheck) {
        hienThiThongTin(vPersonObj);
    }

}
// clear
function onBtnClear() {
    $("#p-html-log").html(">");

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function thuThapThongTin(paramPerson) {

    paramPerson.firstName = $("#inp-first-name").val();

    paramPerson.lastName = $("#inp-last-name").val();
    paramPerson.age = $("#inp-age").val();

}
// ham kiem tra thong tin
function kiemTraThongTin(paramPerson) {
    if (paramPerson.firstName == "" || paramPerson.lastName == "" || paramPerson.age == "") {
        alert("cac truong khong duoc bo trong");
        return false;

    }
    else if (paramPerson.age < 0 || paramPerson.age > 200) {
        alert("Tuổi lớn hơn 0 và nhỏ hơn 200");
        return false;
    }
    return true;
}
// hàm hiển thị thông tin
function hienThiThongTin(paramPerson) {
    $("#p-html-log").html("FirstName: " + paramPerson.firstName + "</br>");
    $("#p-html-log").append("lastName: " + paramPerson.lastName + "</br>");
    $("#p-html-log").append("Age: " + paramPerson.age);

}